
import os
import sys
from itertools import permutations

projectBaseDir = os.path.dirname(os.path.realpath(__file__)) + "/../"

sys.path.insert( 0, projectBaseDir )
sys.path.insert( 0, projectBaseDir + "Tableau" )

from Tableau import *


# ------ Testing ------
w = word( [ 3,1,4,5,2 ] )

print( w )

P, Q = RSK(w)

print( P, Q )

print( f'L(w,1) = {L(w,1)}' )
print( f'L(w,2) = {L(w,2)}' )
print( f'L(w,3) = {L(w,3)}' )


# ------ Problem 1 ------

maybes = []

for i in permutations( range(1,7) ):
    w = word( list(i) )
    if L( w, 1) != 4:
        continue
    if L( w, 2) != 6:
        continue
    maybes.append( w )
    print( w )


print( "The following words have L(w,1) = 4 and L(w,2) = 6, but do not have disjoint increasing subsequences of lengths 2,4." )
for w in maybes:
    P, Q = RSK( w )
    lengths = [ len(s) for s in w.segments() ]
    if 2 in lengths or 4 in lengths:
        continue
    print( w )
        



