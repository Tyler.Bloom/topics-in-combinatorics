
import os
import sys
from itertools import permutations

projectBaseDir = os.path.dirname(os.path.realpath(__file__)) + "/../"

sys.path.insert( 0, projectBaseDir )
sys.path.insert( 0, projectBaseDir + "Tableau" )

from tableau import *


# ------------------ Problem 1 ------------------
print( "# ------------------ Problem 1 ------------------" )

digest = [ ]

for s in list( permutations( range(1,5) ) ):
    s = list(s)
    digest.append( str(tableau( [] ) + s) )
    print( f'[] + {s} = {digest[-1]}' )

unique = sorted(set(digest))
unique = sorted(unique, key=lambda x: x.rfind("["))
unique = sorted(unique, key=lambda x: x.index("]"))

print( "\n" )
for tab in unique:
    print( f'{tab}: {sum([1 for t in digest if t == tab])}' )

# ------------------ Problem 2 ------------------
print( "\n\n# ------------------ Problem 2 ------------------" )

seed = [ [ 2, 2, 3, 5, 5, 7, 7 ],
         [ 3, 3, 4, 6, 7, 8 ],
         [ 4, 5, 5, 8, 8 ],
         [ 6, 6, 6, 9 ],
         [ 7, 8, 8 ],
         [ 8 ] ] 
         
def something( tab: tableau, shape: List[int], size: int = 0, digest: List[int] = [] ):
    if size == 0:
        size = sum(tab.shape()) - sum(shape)
        if size <= 0:
            return None

    if size == len(digest):
        if tab.shape() != shape: return None
        print( tab )
        print( list(reversed(digest)) )
        return digest
    
    out = [ ]
    dummy = tableau()
    for i in range(len(tab.values)):
        dummy.values = [ row.copy() for row in tab.values ]
        val = dummy.unInsert( i )
        if len(digest) == 0:
            newDigs = something( dummy, shape, size, digest + [ val ] )
        elif digest[-1] < val:
            newDigs = something( dummy, shape, size, digest + [ val ] )
        else:
            return None

        if type(newDigs) is None:
            return None

something( tableau( seed ), [6, 6, 5, 3, 2] )    

print( "" )
print( tableau( [ [2, 2, 5, 5, 7, 7], [3, 3, 6, 6, 8, 8], [4, 5, 7, 8, 9], [6, 6, 8], [8, 8] ] ) )
print( "" )
print( tableau( [ [2, 2, 5, 5, 7, 7], [3, 3, 6, 6, 8, 8], [4, 5, 7, 8, 9], [6, 6, 8], [8, 8] ] ) +  7 )
print( tableau( [ [2, 2, 5, 5, 7, 7], [3, 3, 6, 6, 8, 8], [4, 5, 7, 8, 9], [6, 6, 8], [8, 8] ] ) + [7, 5] )
print( tableau( [ [2, 2, 5, 5, 7, 7], [3, 3, 6, 6, 8, 8], [4, 5, 7, 8, 9], [6, 6, 8], [8, 8] ] ) + [7, 5, 4] )
print( tableau( [ [2, 2, 5, 5, 7, 7], [3, 3, 6, 6, 8, 8], [4, 5, 7, 8, 9], [6, 6, 8], [8, 8] ] ) + [7, 5, 4, 3] )
print( "" )
print( tableau( seed ) )
print( (tableau( [ [2, 2, 5, 5, 7, 7], [3, 3, 6, 6, 8, 8], [4, 5, 7, 8, 9], [6, 6, 8], [8, 8] ] ) + [7, 5, 4, 3]) == tableau( seed ) )


# ------------------ Problem 3 ------------------
#print( "\n\n# ------------------ Problem 3 ------------------" )


