
import os
import sys
from itertools import permutations

projectBaseDir = os.path.dirname(os.path.realpath(__file__)) + "/../"

sys.path.insert( 0, projectBaseDir )
sys.path.insert( 0, projectBaseDir + "Tableau" )

from Tableau import *


def changeFilling( tab: tableau, filling: List[int] ) -> tableau:
    digest = tableau( [] )
    digest.values = tab.values.copy()
    index = 0
    for row in digest.values:
        for i in range(len(row)):
            if row[i] is None: continue
            row[i] = filling[index]
            index += 1
    return digest

def bonusProblem( k: int, d: int, probs: List[int], _debug: int = 0 ) -> float:
    #print( k, d, probs )
    print( f'Recur level: {_debug}', probs )
    assert k <= d
    if 0 in [ k, len(probs) ]:
        return 1.0
    digest = 1
    for i in range(len(probs)):
        term = 0
        for j in range(1,d-k+2):
            term += probs[i]**j * bonusProblem( k-1, d-j, probs[:i] + probs[i+1:], _debug = _debug+1 )
        digest *= term
    return digest


# ------ Testing ------

print( "------ Testing ------" )
filling = [ [0, 0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0 ]             ]

T = tableau( filling )
print( T )
T.skew( [3,1] )
print( T )

# ------ Problem 1 ------

print( " ------ Problem 1 ------ " )

filling = [ [0, 0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0 ]             ]

T = tableau( filling )
T.skew( [3,1] )

test = tableau()

maybes = []

for rw in permutations( [ 1, 1, 2, 2, 2, 3 ] ):
    rw = list(rw)
    index = rw.index(3)
    if index != 0 and index != 4 and index != 5:
        continue
    rw.insert(0, 1)
    rw.insert(1, 1)
    rw.insert(4, 2)
    rw.insert(8, 3)
    if rw[6] == 1 or rw[7] == 1 or rw[9] == 1:
        continue
    if rw[2] == 3 or rw[3] == 3 or rw[5] == 3:
        continue
    test = changeFilling( T, rw )
    if not test.isValid():
        continue
    if test in maybes:
        continue
    maybes.append( tableau( test.values.copy() ) )

for t in maybes:
    print( t, t.readingWord() )


# ------ Extra Problem ------


