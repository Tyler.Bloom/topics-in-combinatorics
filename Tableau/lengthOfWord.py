
from word import word
from tableau import *
from RSK import *

def L( w: word, k: int) -> int:
    P = RSK( w )[0]
    digest = 0
    if k >= len(P.values):
        return sum( len(row) for row in P.values )
    for i in range(k):
        digest += len(P.values[i])
    return digest
