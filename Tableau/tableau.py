
from typing import List, Tuple

from word import word

class tableau:
    # ---------------- Methods Handled by Python ----------------
    def __init__( self, table: List[List[int]] = [] ):
        self.values = [ [ i for i in row ] for row in table ]
        self.newBox = (-1,-1)
        self.skewTable = None
    
    def __hash__( self ):
        return 1
    
    def __str__( self ):
        return ", ".join( [ str(row) for row in self.values ] )
    
    def __getitem__( self, item ):
        return self.values[item]
    
    def __eq__( self, other: 'tableau' ):
        if len(self.values) != len(other.values):
            return False
        for i in range(len(self.values)):
            if len(self.values[i]) != len(other.values[i]):
                return False
        for i in range(len(self.values)):
            for j in range(len(self.values[i])):
                if self.values[i][j] != other.values[i][j]:
                    return False
        return True
    
    def __add__( self, val ) -> 'tableau':
        if type(val) is int:
            return self.insert( val )
        elif type(val) is list or type(val) is word:
            if len(val) == 0:
                return self
            return self.insert( val[0] ) + val[1:]
    
    # ---------------- Queries About the Tableau ----------------
    def isValid( self ) -> bool:
        if len(self.values) == 0:
            return True
        
        s = self.shape()
        for i in range(1,len(s)):
            if s[i] > s[i-1]: return False
        
        for row in self.values:
            for i in range(1,len(row)):
                if row[i] is None: continue
                if row[i-1] is None: continue
                if row[i] < row[i-1]: return False

        for i in range(1,len(self.values)):
            for j in range(len(self.values[i])):
                if self.values[i][j] is None: continue
                if self.values[i-1][j] is None: continue
                if self.values[i][j] <= self.values[i-1][j]: return False
        return True
    
    def find( self, val: int ) -> Tuple[int, int]:
        for i in range(len(self.values)):
            if val in self.values[i]:
                return (i,self.values[i].index( val ))
        return (-1,-1)
    
    # ---------------- Psuedo-Properties of the Tableau ----------------
    def shape( self ) -> List[int]:
        return [ len(row) for row in self.values ]
    
    def readingWord( self ) -> word:
        digest = []
        for row in reversed(self.values):
            digest += [ i for i in row if not i is None ]
        return word( digest )
        
    # ---------------- Functions of the Tableau ----------------
    def skew( self, skewTable = List[int] ):
        assert len(skewTable) <= len(self.values)
        for i in range(1,len(skewTable)):
            assert skewTable[i] <= skewTable[i-1]
        for i in range(len(skewTable)):
            assert skewTable[i] <= len(self.values[i])
        self.skewTable = skewTable
        for row in range(len(skewTable)):
            for col in range(skewTable[row]):
                self.values[row][col] = None
        
    def insert( self, val, row: int = 0 ) -> 'tableau':
        # Have we reached the end of the table? Yes? Make a new row
        if row >= len(self.values):
            self.values.append( [ val ] )
            self.newBox = (len(self.values)-1, len(self.values[-1])-1)
            return self
        
        for i in range(len(self.values[row])):
            if val < self.values[row][i]:
                tmp = self.values[row][i]
                self.values[row][i] = val 
                return self.insert( tmp, row+1 )
        
        self.values[row].append( val )
        self.newBox = (row, len(self.values[row])-1)
        return self
    
    def unInsert( self, row: int, val: int = 0 ) -> int:
        if row < 0:
            return val

        if val == 0:
            val = self.values[row][-1]
            del( self.values[row][-1] )
            if len(self.values[row]) == 0:
                del( self.values[row] )
            return self.unInsert( row - 1, val )

        for i in reversed(range(len(self.values[row]))):
            if val > self.values[row][i]:
                tmp = self.values[row][i]
                self.values[row][i] = val
                return self.unInsert( row - 1, tmp )
            



