
from typing import List, Tuple

from tableau import tableau
from word import word

class skewTableau(tableau):
    # ---------------- Methods Handled by Python ----------------
    def __init__( self, table: List[List[int]] = [] ):
        self.values = [ l.copy() for l in table ]
        self.newBox = (-1,-1)
    
    def __hash__( self ):
        return 1
    
    def __str__( self ):
        return ", ".join( [ str(row) for row in self.values ] )
    
    def __getitem__( self, item ):
        return self.values[item]
    
    def __eq__( self, other: 'tableau' ):
        if len(self.values) != len(other.values):
            return False
        for i in range(len(self.values)):
            if len(self.values[i]) != len(other.values[i]):
                return False
        for i in range(len(self.values)):
            for j in range(len(self.values[i])):
                if self.values[i][j] != other.values[i][j]:
                    return False
        return True
    
    def __add__( self, val ) -> 'tableau':
        if type(val) is int:
            return self.insert( val )
        elif type(val) is list or type(val) is word:
            if len(val) == 0:
                return self
            return self.insert( val[0] ) + val[1:]
    
    # ---------------- Queries About the Tableau ----------------
    def isValid( self ) -> bool:
        if len(self.values) == 0:
            return True
        
        for row in self.values:
            for i in range(1,len(row)):
                if row[i] < row[i-1]: return False

        for i in range(len(self.values[0])):
            for j in range(1,len(self.values)):
                try:
                    if self.values[j][i] <= self.values[j-1][i]: return False
                except:
                    break
        return True
    
    def find( self, val: int ) -> Tuple[int, int]:
        for i in range(len(self.values)):
            if val in self.values[i]:
                return (i,self.values[i].index( val ))
        return (-1,-1)
    
    # ---------------- Psuedo-Properties of the Tableau ----------------
    def shape( self ) -> List[int]:
        return [ len(row) for row in self.values ]
    
    def readingWord( self ) -> word:
        digest = []
        for l in self.values:
            digest += l
        return word( digest )
        
    # ---------------- Functions of the Tableau ----------------
    def insert( self, val, row: int = 0 ) -> 'tableau':
        # Have we reached the end of the table? Yes? Make a new row
        if row >= len(self.values):
            self.values.append( [ val ] )
            self.newBox = (len(self.values)-1, len(self.values[-1])-1)
            return self
        
        for i in range(len(self.values[row])):
            if val < self.values[row][i]:
                tmp = self.values[row][i]
                self.values[row][i] = val 
                return self.insert( tmp, row+1 )
        
        self.values[row].append( val )
        self.newBox = (row, len(self.values[row])-1)
        return self
    
    def unInsert( self, row: int, val: int = 0 ) -> int:
        if row < 0:
            return val

        if val == 0:
            val = self.values[row][-1]
            del( self.values[row][-1] )
            if len(self.values[row]) == 0:
                del( self.values[row] )
            return self.unInsert( row - 1, val )

        for i in reversed(range(len(self.values[row]))):
            if val > self.values[row][i]:
                tmp = self.values[row][i]
                self.values[row][i] = val
                return self.unInsert( row - 1, tmp )
            



