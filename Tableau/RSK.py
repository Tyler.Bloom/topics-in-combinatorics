
from typing import Tuple

from word import word
from tableau import tableau
from wordToTableau import *


def RSK( w: word ) -> Tuple[tableau, tableau]:
    P = tableau()
    Q = tableau()
    
    for i in range(len(w)):
        P += w[i]
        row, col = P.newBox
        if row >= len(Q.values):
            Q.values.append([])
        Q.values[row].append( i + 1 )
    
    return ( P, Q )
    


