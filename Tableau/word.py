
from typing import List

class word:
    def __init__( self, vals: List[int] = [] ):
        self.values = vals.copy()
    
    def __str__( self ) -> str:
        return str( self.values )
    
    def __getitem__( self, item ) -> int:
        return self.values[item]
    
    def __len__( self ) -> int:
        return len(self.values)
    
    def segments( self ) -> List[int]:
        digest = [ [] ]
        for i in range(1,len(self.values)):
            if self.values[i] < self.values[i-1]:
                digest.append( [] )
            digest[-1].append( self.values[i] )
        return digest
     
    def discontinuities( self ) -> int:
        digest = 0
        for i in range(1,len(self.values)):
            if self.values[i] < self.values[i-1]:
                digest += 1
        return digest
